package Controllers;

import DAO.ProcessingDB;
import DAO.UtilisateurDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class AutentificationController {

    private UtilisateurDAO utilisateurDAO;
    private String nomUtilisateurToSend;


    public String getnomUtilisateurToSend() {
        return nomUtilisateurToSend;
    }

    public void setnomUtilisateurToSend(String nomUtilisateurToSend) {
        this.nomUtilisateurToSend = nomUtilisateurToSend;
    }

    @FXML // fx:id="pswdInput"
    private PasswordField pswdInput; // Value injected by FXMLLoader

    @FXML // fx:id="mainLoginBtn"
    private Button mainLoginBtn; // Value injected by FXMLLoader

    @FXML // fx:id="nomUtilisateurInput"
    private TextField nomUtilisateurInput; // Value injected by FXMLLoader

    @FXML
    private Hyperlink passAuth;

    @FXML
    private Hyperlink signUp;


    public AutentificationController() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.utilisateurDAO = processingDB.getUtilisateurDAO();

    }

    public UtilisateurDAO getUtilisateurDAO() {
        return utilisateurDAO;
    }

    public void initialize() throws IOException {



    }



    @FXML
    void login(ActionEvent event) throws SQLException, IOException {

        String nomUtilisateurInputText = nomUtilisateurInput.getText();
        String pswdInputText = pswdInput.getText();

        if(utilisateurDAO.loginVerif(nomUtilisateurInputText, pswdInputText)) {
            setnomUtilisateurToSend(nomUtilisateurInputText);
            //On récupère la fenêtre de login
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            //On la ferme
            stage.close();
            //On redirige vers la fenêtre des annonces
            Stage st = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/annoncesUser.fxml"));
            Region root = loader.load();

            Scene scene = new Scene(root);
            st.setTitle("Gestion d'annonces");
            st.setScene(scene);

            //On récupère le nomUtilisateur pour l'afficher
            MainControllerUser mainControllerUser = loader.<MainControllerUser>getController();
            mainControllerUser.getLabelnomUtilisateur().setText(nomUtilisateurInput.getText());

            st.setResizable(false);
            st.show();
        }
        else {
            allertDialog("Erreur !", Alert.AlertType.ERROR, "Nom d'utilisateur ou mot de passe incorrect, veuillez réessayer");
        }
    }

    @FXML
    void signUp(ActionEvent event) throws IOException {

        //On redirige vers la fenêtre d'inscription

        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Inscription.fxml"));
        Region root = loader.load();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setTitle("Inscription");
        st.setResizable(false);
        st.show();

    }

    @FXML
    void passAuthentification(ActionEvent event) throws IOException {
        //On récupère la fenêtre de login
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        //On la ferme
        stage.close();
        //On redirige vers la fenêtre des annonces pour les visiteurs
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/annoncesVisitor.fxml")));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Gestion d'annonces (Mode visiteur)");
        stage.show();

    }

    //Méthode gérant les alertes
    private void allertDialog(String header,Alert.AlertType alertType, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle("Dialog info");
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }






}

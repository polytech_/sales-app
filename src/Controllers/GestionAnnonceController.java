package Controllers;

import DAO.AnnonceDAO;
import DAO.CategorieDAO;
import DAO.ProcessingDB;
import Model.Annonce;
import Model.Categorie;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.function.Predicate;

public class GestionAnnonceController {

    private AnnonceDAO annonceDAO;
    private CategorieDAO categorieDAO;
    private MainControllerUser mainControllerUser;

    public void setMainControllerUser(MainControllerUser mainControllerUser) {
        this.mainControllerUser = mainControllerUser;
    }

    private ObservableList<Annonce> donnees;

    public ObservableList<Annonce> getDonnees() {
        return donnees;
    }
    public void setDonnees(ObservableList<Annonce> donnees) {
        this.donnees = donnees;
    }


    @FXML // fx:id="GAcolumnCat"
    private TableColumn<Annonce, String> GAcolumnCat; // Value injected by FXMLLoader

    @FXML // fx:id="GAcolumnPos"
    private TableColumn<Annonce, String> GAcolumnPos; // Value injected by FXMLLoader

    @FXML // fx:id="GAcolumnPrice"
    private TableColumn<Annonce, Float> GAcolumnPrice; // Value injected by FXMLLoader


    @FXML // fx:id="GAcolumnUser"
    private TableColumn<Annonce, String> GAcolumnUser; // Value injected by FXMLLoader


    @FXML // fx:id="GAcolumnDesc"
    private TableColumn<Annonce, String> GAcolumnDesc; // Value injected by FXMLLoader

    @FXML // fx:id="removeAnnounceBtn"
    private Button removeAnnounceBtn; // Value injected by FXMLLoader

    @FXML // fx:id="annoncesTBV"
    private TableView<Annonce> annoncesTBV; // Value injected by FXMLLoader

    @FXML // fx:id="GAlocationCBX"
    private ComboBox<String> GAlocationCBX; // Value injected by FXMLLoader

    @FXML // fx:id="GAlabelnomUtilisateur"
    private Label GAlabelnomUtilisateur; // Value injected by FXMLLoader

    public Label getGAlabelnomUtilisateur() {
        return GAlabelnomUtilisateur;
    }

    @FXML // fx:id="GAcategoryCBX"
    private ComboBox<String> GAcategoryCBX; // Value injected by FXMLLoader

    @FXML // fx:id="GAcolumnType"
    private TableColumn<Annonce, String> GAcolumnType; // Value injected by FXMLLoader


    @FXML
    private TextArea descInput;
    @FXML
    private TextField posInput;
    @FXML
    private TextField priceInput;
    @FXML
    private TextField catgryInput;
    @FXML
    private TextField nomUtilisateurField;

    @FXML
    private Button filtresOff;


    public TextField getnomUtilisateurField() {
        return nomUtilisateurField;
    }

    @FXML
    private TextField transTypeInput;





    public GestionAnnonceController() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.annonceDAO = processingDB.getAnnonceDAO();
        this.categorieDAO = processingDB.getCategorieDAO();
    }


    public void initialize() throws IOException, SQLException {


    }


    @FXML
    void ajoutAnnonce(ActionEvent event) throws IOException, SQLException {


        if(categorieDAO.selectionCategorie(catgryInput.getText())) {
            Annonce annonce = new Annonce();
            annonce.setCategorie(catgryInput.getText());
            annonce.setTypeTransaction(transTypeInput.getText());
            annonce.setPrix(Float.parseFloat(priceInput.getText()));
            annonce.setDescription(descInput.getText());
            annonce.setPosition(posInput.getText());
            annonce.setUtilisateur(nomUtilisateurField.getText());

            //On vérifie si l'annonce existe déjà
            if(!annonceDAO.verifExistenceAnnonce(annonce)) {
                annonceDAO.ajoutAnnonce(annonce);
                donnees.add(annonce);
                mainControllerUser.getDonnees().add(annonce);
            }
            else {
                allertDialog("Erreur !", Alert.AlertType.ERROR, "L'annonce existe déjà !");
            }

        }

        else {
            Categorie categorie = new Categorie();
            categorie.setName(catgryInput.getText());

            Annonce annonce = new Annonce();
            annonce.setCategorie(catgryInput.getText());
            annonce.setTypeTransaction(transTypeInput.getText());
            annonce.setPrix(Float.parseFloat(priceInput.getText()));
            annonce.setDescription(descInput.getText());
            annonce.setPosition(posInput.getText());
            annonce.setUtilisateur(nomUtilisateurField.getText());
            categorieDAO.ajoutCategorie(categorie);
            //On vérifie si l'annonce existe déjà
            if(!annonceDAO.verifExistenceAnnonce(annonce)) {
                annonceDAO.ajoutAnnonce(annonce);
                donnees.add(annonce);
                mainControllerUser.getDonnees().add(annonce);
            }
            else {
                allertDialog("Erreur !", Alert.AlertType.ERROR, "L'annonce existe déjà !");

            }
        }
    }

    @FXML
    void supprAnnonce(ActionEvent event) throws SQLException, IOException {

        Annonce annonceSelectionnee = annoncesTBV.getSelectionModel().getSelectedItem();
        int idAnnonceSelectionnee = annoncesTBV.getSelectionModel().getSelectedItem().getId();

        annonceDAO.suppressionAnnonce(idAnnonceSelectionnee);

        if (annonceSelectionnee != null) {
            donnees.remove(annonceSelectionnee);
        }

        mainControllerUser.getDonnees().remove(annonceSelectionnee);

    }



    public void remplissage() throws SQLException {

        setDonnees(FXCollections.observableArrayList(annonceDAO.listerAnnoncesParUtilisateur(GAlabelnomUtilisateur.getText())));

        GAcolumnUser.setCellValueFactory(new PropertyValueFactory<>("Utilisateur"));
        GAcolumnPos.setCellValueFactory(new PropertyValueFactory<>("Position"));
        GAcolumnType.setCellValueFactory(new PropertyValueFactory<>("TypeTransaction"));
        GAcolumnCat.setCellValueFactory(new PropertyValueFactory<>("Categorie"));
        GAcolumnPrice.setCellValueFactory(new PropertyValueFactory<>("Prix"));
        GAcolumnDesc.setCellValueFactory(new PropertyValueFactory<>("Description"));


        FilteredList<Annonce> filterData = new FilteredList<>(donnees);
        SortedList<Annonce> personSortedList = new SortedList<>(filterData);
        annoncesTBV.setItems(personSortedList);

        personSortedList.comparatorProperty().bind(annoncesTBV.comparatorProperty());



        //Filtre des catégories
        ObservableList<String> filterCategories = FXCollections.observableArrayList(annonceDAO.listerCategories());
        GAcategoryCBX.getItems().addAll(filterCategories);

        //Filtre de localisation
        ObservableList<String> filterLocalisations = FXCollections.observableArrayList(annonceDAO.listerLocalisations());
        GAlocationCBX.getItems().addAll(filterLocalisations);

        ObjectProperty<Predicate<Annonce>> catFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Annonce>> locFilter = new SimpleObjectProperty<>();


        //On lie les deux filtres
        catFilter.bind(Bindings.createObjectBinding(() ->
                        annonce -> GAcategoryCBX.getValue() == null || GAcategoryCBX.getValue().equals(annonce.getCategorie()),
                GAcategoryCBX.valueProperty()));
        locFilter.bind(Bindings.createObjectBinding(() ->
                        annonce -> GAlocationCBX.getValue() == null || GAlocationCBX.getValue().equals(annonce.getPosition()),
                GAlocationCBX.valueProperty()));


        //On applique les deux filtres à la table
        filterData.predicateProperty().bind(Bindings.createObjectBinding(
                () -> catFilter.get().and(locFilter.get()),
                catFilter, locFilter));


        //Suppression des filtres à l'aide du bouton sans filtres
        filtresOff.setOnAction(e -> {
            GAlocationCBX.setValue(null);
            GAcategoryCBX.setValue(null);
        });

    }

    //Méthode gérant les alertes
    private void allertDialog(String header,Alert.AlertType alertType, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle("Dialog info");
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }





}

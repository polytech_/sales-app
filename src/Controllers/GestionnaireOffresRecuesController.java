package Controllers;

import DAO.AnnonceDAO;
import DAO.OffreDAO;
import DAO.ProcessingDB;
import Model.Annonce;
import Model.Offre;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.SQLException;
import java.util.Iterator;


public class GestionnaireOffresRecuesController {

    private OffreDAO offreDAO;
    private ObservableList<Offre> donnees;
    private MainControllerUser mainControllerUser;

    public void setMainControllerUser(MainControllerUser mainControllerUser) {
        this.mainControllerUser = mainControllerUser;
    }


    public ObservableList<Offre> getDonnees() {
        return donnees;
    }

    @FXML
    private Label labelnomUtilisateur;

    public Label getLabelnomUtilisateur() {
        return labelnomUtilisateur;
    }

    @FXML
    private TableView<Offre> offresTBV;

    @FXML
    private Button accBtn;

    @FXML
    private Button rejBtn;

    @FXML
    private TableColumn<Offre, String> stateCol;

    @FXML
    private TableColumn<Offre, String> userCol;

    @FXML
    private TableColumn<Offre, Float> priceCol;


    public GestionnaireOffresRecuesController() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.offreDAO = processingDB.getOffreDAO();
    }



    public void initialize() {


    }



    public void remplissage() throws SQLException {

        donnees = FXCollections.observableArrayList(offreDAO.listerOffresRecues(labelnomUtilisateur.getText()));

        userCol.setCellValueFactory(new PropertyValueFactory<>("sujet"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("Prix"));
        stateCol.setCellValueFactory(new PropertyValueFactory<>("Etat"));

        offresTBV.setItems(donnees);

        Iterator iterator = donnees.iterator();
        while(iterator.hasNext()) {
            if(!((Offre)iterator.next()).getEtat().equals("En attente")) {
                accBtn.setDisable(true);
                rejBtn.setDisable(true);
            }

        }


    }



    @FXML
    void rejeterOffre() throws SQLException {

        //On récupère l'offre reçue :
        int index = offresTBV.getSelectionModel().getSelectedIndex();
        Offre offre = offresTBV.getItems().get(index);

        //On la rejette
        offreDAO.rejetOffre(offre);

        //On change l'état dans la table
        offre.setEtat("Rejetée");
        accBtn.setDisable(true);
        rejBtn.setDisable(true);

        offresTBV.refresh();
    }



    @FXML
    void accepterOffre(ActionEvent event) throws SQLException {
        int index = offresTBV.getSelectionModel().getSelectedIndex();
        Offre offre = offresTBV.getItems().get(index);
        ObservableList<Annonce> annoncesCibles = offreDAO.selectionAnnoncesCibles(offre.getSujet());
        offreDAO.acceptationOffre(offre);

        offre.setEtat("Acceptée");
        accBtn.setDisable(true);
        rejBtn.setDisable(true);

        offresTBV.refresh();

        //On vire l'annonce de la table mais non pas de la base de données
        // car la contrainte de clé étrangère nous en empêche
        annoncesCibles.forEach(annonce -> mainControllerUser.getDonnees().remove(annonce));
        mainControllerUser.getAnnoncesTBV().refresh();


    }
}


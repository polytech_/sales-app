package Model;

public class Categorie {

    private String name;

    public Categorie() {
    }

    public Categorie(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}

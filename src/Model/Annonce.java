package Model;

import javafx.beans.property.StringProperty;

public class Annonce {

    private int id;
    private String categorie;
    private String typeTransaction;
    private float prix;
    private String description;
    private String position;
    private String utilisateur;

    public Annonce() {

    }



    public Annonce(int id,
                   String categorie,
                   String typeTransaction,
                   float prix,
                   String description,
                   String position,
                   String utilisateur) {

        this.id = id;
        this.categorie = categorie;
        this.typeTransaction = typeTransaction;
        this.prix = prix;
        this.description = description;
        this.position = position;
        this.utilisateur = utilisateur;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getTypeTransaction() {
        return typeTransaction;
    }

    public void setTypeTransaction(String typeTransaction) {
        this.typeTransaction = typeTransaction;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }


    //Override de la méthode equals à cause du fait que les annonces ne sont pas reconnues dans la table
    @Override
    public boolean equals(Object obj) {
        return ((((Annonce)obj).id == this.id && ((Annonce)obj).categorie.equals(this.categorie)) &&
                 ((Annonce)obj).prix == this.prix && ((Annonce)obj).utilisateur.equals(this.utilisateur)
                && ((Annonce)obj).typeTransaction.equals(this.typeTransaction));
    }
}

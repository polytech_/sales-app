package DAO;

import Model.Annonce;
import Model.Offre;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

import javax.swing.plaf.nimbus.State;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OffreDAO {

    private ProcessingDB processingDB;

    public OffreDAO(ProcessingDB processingDB) {
        this.processingDB = processingDB;
    }


    public List<Offre> listerOffresRecues(String utilisateur) throws SQLException {
        ResultSet resultSet = null;

        ArrayList<Offre> listOffres = new ArrayList<>();

        resultSet = processingDB.getConnection().createStatement().
                executeQuery("SELECT offre.annonce, annonce.utilisateur, offre.sujet, offre.prix, offre.etat\n" +
                              "   FROM offre\n" +
                              "   INNER JOIN annonce on offre.annonce = annonce.Id\n" +
                              "   INNER JOIN utilisateur on annonce.utilisateur = utilisateur.nomUtilisateur\n" +
                              "   WHERE utilisateur.nomUtilisateur="+"'"+utilisateur+"' AND offre.etat <> 'Rejetée';");

        while(resultSet.next()) {
            listOffres.add(new Offre(
                    resultSet.getInt("annonce"),
                    resultSet.getString("utilisateur"),
                    resultSet.getString("sujet"),
                    resultSet.getFloat("prix"),
                    resultSet.getString("etat")));
        }

        return listOffres;

    }

    public void ajoutOffre(Offre offre) throws SQLException {
        PreparedStatement preparedStatement = null;

        preparedStatement = processingDB.getConnection().prepareStatement("INSERT INTO offre(annonce, sujet, prix) VALUES(?,?,?)");

        preparedStatement.setInt(1, offre.getIdAnnonce());
        preparedStatement.setString(2, offre.getSujet());
        preparedStatement.setFloat(3, offre.getPrix());



        preparedStatement.executeUpdate();

        preparedStatement.close();

    }



    public boolean selectionOffre(int idAnnonce) throws SQLException {

        ResultSet resultset = null;

        resultset = processingDB.getConnection().createStatement().executeQuery("SELECT * from offre where annonce="+idAnnonce);

        return resultset.next();
    }



    public List<Offre> listerOffresEmises(String sujet) throws SQLException {
        ResultSet resultSet = null;

        ArrayList<Offre> listOffres = new ArrayList<>();

        resultSet = processingDB.getConnection().createStatement()
                .executeQuery("SELECT annonce.utilisateur, offre.prix, offre.etat" +
                        "   FROM offre" +
                        "   INNER JOIN annonce on offre.annonce = annonce.Id"+
                        "   WHERE offre.sujet="+"'"+sujet+"';");

        while(resultSet.next()) {
            listOffres.add(new Offre(
                    resultSet.getString("utilisateur"),
                    resultSet.getFloat("prix"),
                    resultSet.getString("etat")));
        }

        return listOffres;
    }



    public ObservableList<Annonce> selectionAnnoncesCibles(String sujet) throws SQLException {

        ResultSet resultSet = null;

        ArrayList<Annonce> listAnnoncesCibles = new ArrayList<>();

        resultSet = processingDB.getConnection().createStatement().
                executeQuery("SELECT *" +
                        "   FROM annonce" +
                        "   INNER JOIN offre on annonce.Id=offre.annonce"+
                        "   WHERE offre.sujet="+"'"+sujet+"' AND offre.etat <> 'Rejetée';");

        while(resultSet.next()) {
            listAnnoncesCibles.add(new Annonce(
                    resultSet.getInt("Id"),
                    resultSet.getString("Categorie"),
                    resultSet.getString("TypeTransaction"),
                    resultSet.getFloat("Prix"),
                    resultSet.getString("Description"),
                    resultSet.getString("Position"),
                    resultSet.getString("utilisateur")));
        }

        return FXCollections.observableArrayList(listAnnoncesCibles);

    }


    public void rejetOffre(Offre offre) throws SQLException {

        PreparedStatement preparedStatement = processingDB.getConnection().prepareStatement(" UPDATE offre" +
                " SET offre.etat = 'Rejetée'"+
                " WHERE offre.annonce ="+offre.getIdAnnonce()+";");

        preparedStatement.executeUpdate();
    }

    public void acceptationOffre(Offre offre) throws SQLException {
        PreparedStatement preparedStatement = processingDB.getConnection().prepareStatement(" UPDATE offre" +
                " SET offre.etat = 'Acceptée'"+
                " WHERE offre.annonce ="+offre.getIdAnnonce()+";");

        preparedStatement.executeUpdate();

    }








}

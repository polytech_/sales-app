package DAO;

import Model.Utilisateur;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UtilisateurDAO {

    private ProcessingDB processingDB;


    public UtilisateurDAO(ProcessingDB processingDB) {
        this.processingDB = processingDB;
    }

    public void inscription(Utilisateur utilisateur) throws SQLException {

        PreparedStatement preparedStatement = null;

        preparedStatement = processingDB.getConnection().prepareStatement("INSERT INTO utilisateur VALUES(?,?,?,?)");

        preparedStatement.setString(1, utilisateur.getFullName());
        preparedStatement.setString(2, utilisateur.getnomUtilisateur());
        preparedStatement.setString(3, utilisateur.getmotDePasse());
        preparedStatement.setString(4, utilisateur.getPhoneNumber());

        preparedStatement.executeUpdate();

        preparedStatement.close();

    }


    public boolean loginVerif(String nomUtilisateur, String passwd) throws SQLException {
        PreparedStatement preparedStatement = null;

        preparedStatement = processingDB.getConnection().prepareStatement("SELECT * FROM utilisateur where nomUtilisateur=? and motDePasse=?");

        preparedStatement.setString(1, nomUtilisateur);
        preparedStatement.setString(2, passwd);

        ResultSet resultSet = preparedStatement.executeQuery();

        return resultSet.next();

    }

    public boolean verifnomUtilisateur(String nomUtilisateur) throws SQLException {
        PreparedStatement preparedStatement = null;

        preparedStatement = processingDB.getConnection().prepareStatement("SELECT * FROM utilisateur where nomUtilisateur=?");

        preparedStatement.setString(1, nomUtilisateur);

        ResultSet resultSet = preparedStatement.executeQuery();

        return resultSet.next();

    }
}

package DAO;

import Model.Utilisateur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProcessingDB {

    private String url;
    private String nomUtilisateur;
    private String motDePasse;
    private String database;


    public ProcessingDB(String url, String nomUtilisateur, String motDePasse, String database) {
        this.url = url;
        this.nomUtilisateur = nomUtilisateur;
        this.motDePasse = motDePasse;
        this.database = database;
    }

    public static ProcessingDB getInstance() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex1) {
            System.out.println("Pilote non trouvé !");
            System.exit(1);
        }
        //Paramètrage de l'accès à la bdd : Merci de remplacer les champs suivants par l'url, l'utilisater, le mot de mot de passe
            // ainsi que le shcema cibles afin de permettre l'application d'acceder à la bdd
        return new ProcessingDB("jdbc:mysql://localhost/","othmane", "Aqwzsxedc123", "salesschema");
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url+database+"?serverTimezone=UTC", nomUtilisateur, motDePasse);
    }

    //Récupération des DAO
    public AnnonceDAO getAnnonceDAO() {
        return new AnnonceDAO(this);
    }

    public CategorieDAO getCategorieDAO() {
        return new CategorieDAO(this);
    }

    public UtilisateurDAO getUtilisateurDAO() {
        return new UtilisateurDAO(this);
    }

    public OffreDAO getOffreDAO() {
        return new OffreDAO(this);
    }








}
